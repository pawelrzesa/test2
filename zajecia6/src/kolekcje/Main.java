package kolekcje;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> imiona = new ArrayList<>();
        imiona.add("Paweł");
        imiona.add("Ania");

        wyswietlNaEkranie(imiona);

        imiona.add(1, "Ola");

        wyswietlNaEkranie(imiona);

        }
        private static void wyswietlNaEkranie(List<?> lista){
        for (Object imie: lista) {
            System.out.println(imie);
        }
    }
}
