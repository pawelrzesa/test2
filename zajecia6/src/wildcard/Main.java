package wildcard;

public class Main {

    class Printer{
        public void print(Czlowiek czlowiek){
            System.out.println(czlowiek.informacje());
        }
    }

    public static void main(String[] args) {
        Czlowiek czlowiek = new Czlowiek();

        Main main = new Main();
        Printer printer = main.new Printer();
        printer.print(czlowiek);
    }
}
