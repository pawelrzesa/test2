package slownik;

import com.sun.corba.se.impl.resolver.SplitLocalResolverImpl;

public class Main {
    public static void main(String[] args) {
        //zaprezentujSlownikAgielskoPolski();
        zaprezentujSlownikZKolorami();
    }

    private static void zaprezentujSlownikZKolorami(){
        Slownik<String, Kolor> kolory = new Slownik<>();
        kolory.dodajWpis("niebieski", new Kolor(0,0,255));
        kolory.dodajWpis("czerwony", new Kolor(255,0,0));
        kolory.dodajWpis("zielony", new Kolor(0,255,0));
        wyswietlKolor("niebieski", kolory);
    }

    private static void wyswietlKolor(String kolor, Slownik<String , Kolor> slownik){
        System.out.printf("Wartości składowe dla koloru %s to %s",kolor, slownik.getWartosc(kolor).wartosciWszystkie());
    }

    private  static void zaprezentujSlownikAgielskoPolski(){
        String polskaNazwa = "kot";
        String angielskaNazwa = "cat";

        Slownik<String, String> slownikPolskoAngielski = new Slownik<>();
        slownikPolskoAngielski.dodajWpis(polskaNazwa, angielskaNazwa);
        slownikPolskoAngielski.dodajWpis("pies", "dog");
        slownikPolskoAngielski.dodajWpis("mysz", "mouse");

        System.out.println("Angielska nazwa to: " + slownikPolskoAngielski.getWartosc(polskaNazwa));
        System.out.printf("Angielska nazwa dla %s to: %s\n","pies", slownikPolskoAngielski.getWartosc("pies"));
    }

    private static void wyswietlIWpiszZeSlownikaJezykowego(String slowo, Slownik<String, String> slownik){

    }
}
