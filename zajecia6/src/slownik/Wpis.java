package slownik;

public class Wpis<K, V> {
    private final K klucz;
    private final V wartosc;

    public Wpis(K klucz, V wartosc) {
        this.klucz = klucz;
        this.wartosc = wartosc;
    }

    public V getWartosc(){
        return wartosc;
    }

    public K getKlucz(){
        return klucz;
    }
}
