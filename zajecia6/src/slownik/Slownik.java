package slownik;

public class Slownik <K, V>{
    private Wpis<K,V>[] elementy = new Wpis[9];
    private int index = 0;

    public void dodajWpis(K klucz, V wartosc){
        if(index >= elementy.length){
            throw new IllegalStateException("Nie można dodać kolejnego wpisu do słownika ponieważ osiągnięto maksymalną ilość wpisów: " + elementy.length);
        }
        elementy[index] = new Wpis<K, V>(klucz, wartosc);
        index++;
    }

    public V getWartosc(K klucz){
        for (Wpis<K, V> rekord:elementy ) {
            if (rekord.getKlucz().equals(klucz)){
                return rekord.getWartosc();
            }
        }
        return null;
    }
}
