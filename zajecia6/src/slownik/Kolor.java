package slownik;

public class Kolor {
    private final int red;
    private final int green;
    private final int blue;

    public Kolor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getBlue() {
        return blue;
    }

    public int getGreen() {
        return green;
    }

    public int getRed() {
        return red;
    }

    public String wartosciWszystkie(){
        return "red "+ red + " green "+ green + " blue "+ blue;
    }
}
