package ZadanieLista;

public class Student {
    private String imie;
    private String nazwisko;
    private int rocznik;

    public Student(String imie, String nazwisko, int rocznik){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.rocznik = rocznik;
    }

    public int getRocznik() {
        return rocznik;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
