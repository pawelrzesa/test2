package ZadanieLista;

import java.util.ArrayList;
import java.util.List;

public class StudentMain<K,V> {
    public static void main(String[] args) {

        List<Student> lista = new ArrayList<>();

        lista.add(new Student("Adam", "Woźniak", 3));
        lista.add(new Student("Paweł", "Rzęsa", 2));
        lista.add(new Student("Ola", "Jakaś", 2));

        for (Student student:lista) {
            if(student.getRocznik() == 2){
                System.out.println("Student o imieniu " + student.getImie() + " nazwisko "+ student.getNazwisko());
            }
        }

    }


}
