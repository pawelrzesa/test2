package ZadanieLista;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        List<String> lista = new ArrayList<>();
        lista.add("masło");
        lista.add("chleb");
        lista.add("woda");
        lista.add("mleko");

        List<String> zrealizowanaLista = new ArrayList<>();

        wyswietlNaEkran(lista);
        System.out.println("--------------------");
        usunZListy(lista);
        wyswietlNaEkran(lista);
        System.out.println("--------------------");
        produktyNaM(lista);
    }

    public static void usunZListy(List<String> lista) {
        Scanner skan = new Scanner(System.in);
        System.out.print("Podaj zakupiony produkt: ");
        String kupiony = skan.next();
        lista.remove(kupiony);
    }

    public static void wyswietlNaEkran(List<String> lista) {
        for (String zakupy : lista) {
            System.out.println(zakupy);
        }
    }

    public static void produktyNaM(List<String> lista){
        for (String zakupy : lista) {
            if(zakupy.startsWith("m")){
                System.out.println("Produkt na literę M: "+ zakupy);
            }
        }
    }

    private static void wyswietlInaczej(List<String> lista){
        String poprzedniProdukt = null;
        for (String produkt :lista ) {
            if(poprzedniProdukt != null && produkt.startsWith("m")){
                System.out.println(poprzedniProdukt);
            }
            poprzedniProdukt = produkt;
        }
    }


}
