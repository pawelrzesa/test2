package generyki;

public class Printer <T extends User>{
    private T user;

    public T getUser() {
        return user;
    }

    public void setUser(T user) {
        this.user = user;
    }
}
