package generyki;

public class PrinterNoGenerric {

    public <T extends User> void print(T user){
        System.out.println("Nazwa: " + user.getName());
    }

    public <T> T process (T user){
        System.out.println(user);
        return user;
    }
}
