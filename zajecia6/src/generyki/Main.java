package generyki;

import java.util.Objects;

public class Main {

    class Czlowiek extends User {
        public Czlowiek(String name) {
            super(name);
        }
    }

    class Student extends User {
        public Student(String name) {
            super(name);
        }
    }

    public static void main(String[] args) {
        Printer <Czlowiek> czlowiekPrinter = new <Czlowiek> Printer();
        Czlowiek czlowiek = czlowiekPrinter.getUser();

        Printer printer = new Printer();
        Object user = printer.getUser();

        Printer<Student> studentPrinter = new <Student> Printer();
        Main main = new Main();
        studentPrinter.setUser(main.new Student("Paweł"));
        Student student = studentPrinter.getUser();

        PrinterNoGenerric printerNoGenerric = new PrinterNoGenerric();
        printerNoGenerric.<Student>print(student);
    }
}
