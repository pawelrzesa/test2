package zadanie1;

public class Porownywarka {
    public static <T extends Comparable> void wyswietlWiekszeElementy(T[] elementy, T elementDoPorownania){
        for (T element:elementy ) {
            if(element.compareTo(elementDoPorownania) > 0){
                System.out.println(element);
            }
        }
    }

    public static void main(String[] args) {
        Integer[] tablica = {1,2,3,4,5,6,7,8,9,0};
        Integer elementDoPorownania = 5;

        Porownywarka.<Integer>wyswietlWiekszeElementy(tablica, elementDoPorownania);

        Double[] tablicaDouble = {1.2, 1.8, 1.4, 4.3, 5.7,};
        Double elementDouble = 2.0;
        Porownywarka.<Double>wyswietlWiekszeElementy(tablicaDouble, elementDouble);
    }
}
