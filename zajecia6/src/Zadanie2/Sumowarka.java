package Zadanie2;
import java.lang.Number;
import java.util.Scanner;

public class Sumowarka {
    public static <T extends Number> void SumaWiekszychElementow (T[] elementy, T elementDoPorownania){
        double suma = 0;
        for (T elementSpr: elementy  ) {
            if(elementSpr.doubleValue() > elementDoPorownania.doubleValue()){
                suma = suma + elementSpr.doubleValue();
            }
        }
        System.out.println("Suma liczb wynosi: " + suma);
    }

    public static void main(String[] args) {

        Integer[] tablica = {1,2,3,4,5,6,7,8,9,0};
        Scanner skan = new Scanner(System.in);
        System.out.print("Podaj liczbę którą mam porównać: ");
        Integer liczb = skan.nextInt();

        Sumowarka.<Integer>SumaWiekszychElementow(tablica, liczb);
    }
}
